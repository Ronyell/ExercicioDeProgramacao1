#include "pgm.hpp"
using namespace std;

Pgm::Pgm(){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco("  ");
  setNumeroMagico("  ");
  setPosicaoInicial(0);
}
Pgm::Pgm(string endereco){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco(endereco);
  setNumeroMagico("  ");
  this->posicaoInicial=0;
}
Pgm::~Pgm(){
}
int Pgm::getPosicaoInicial(){
  return this->posicaoInicial;
}
void Pgm::setPosicaoInicial(int posicaoInicial){
  this->posicaoInicial=posicaoInicial;
}
void Pgm::pegarPosicaoInicial(){
  int contador=0;
  string posicaoInicial;
  for(int contador1=0;contador1<(int)getPixel()->size() && contador<2;contador1++){
    if (getPixel()->at(contador1)=='\n'){
      contador++;
    }
    if (getPixel()->at(contador1)=='#'){
      if(contador==1){
        for(int contador2=contador1+2;getPixel()->at(contador2)!=' ';contador2++){
          posicaoInicial+=getPixel()->at(contador2);
        }
      this->posicaoInicial=stoi(posicaoInicial);
      contador++;
      }
    }
  }
}
int Pgm::calculaPosicaoInicial(){
  int contador1=0,auxiliar;
  for(int contador=0;getPixel()->at(contador)&&contador1<3;contador++){
       if(getPixel()->at(contador)=='\n'){
         contador1++;
       }
       if(getPixel()->at(contador)=='#'){
         contador1--;
       }
       if (contador1==3){
         auxiliar=contador;
       }
     }
  return auxiliar+1+posicaoInicial;
}
void Pgm::extrairMensagem(){
  char pixel,caracter;
  int contador1=0;
  fstream texto;
  string endereco;
  for(int contador=0;contador<(int)strlen(getEndereco().c_str());contador++){
    if(contador<(int)strlen(getEndereco().c_str())-3){
      endereco+=getEndereco()[contador];
    }
    else{
      endereco+="Mensagem.txt";
      contador=(int)strlen(getEndereco().c_str())+1;
    }
  }
  texto.open(endereco.c_str(),ios::out);
  for(int contador=(int)calculaPosicaoInicial();contador1<8;contador++){
  pixel= (getPixel()->at(contador)&0x01);
      caracter= pixel|(caracter<<1);
    if(contador1==7){
        if(caracter!='#'){
          texto<<caracter;
        }
        if(caracter!='#'||contador<calculaPosicaoInicial()+16){
          contador1=-1;
          caracter=caracter&0x00;
      }
    }
    contador1++;
  }
  cout<<"A mensagem foi escrita em um arquivo no diretório: "<<endereco<<endl;
  texto.close();
}
void Pgm::testeNumeroMagico(){
  if (strcmp(getNumeroMagico().c_str(),"P5")){
    cout<<"Erro, arquivo não é Pgm!"<<endl;
    exit(0);
  }
}
