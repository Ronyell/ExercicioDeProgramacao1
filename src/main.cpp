#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "imagem.hpp"
#include "pgm.hpp"
#include "ppm.hpp"
using namespace std;

int main(int argc, char ** argv) {
  int menu;
  do{
      cout<<"=================================== Menu ==================================="<<endl;
      cout<<"Escolha o tipo de imagem que deseja aplicar a esteganografia:\n"<<"1 - Pgm\n"<<"2 - Ppm\n"<<"0 - Sair"<<endl;
      cin>>menu;
      switch(menu){
        case 0:
              cout<<"=== Sair ===";
              break;
        case 1:
            cout<<"=== Pgm ==="<<endl;
            Pgm *imagem1;
            imagem1=new Pgm();
            imagem1->digiteEndereco();
            imagem1->abrirImagemLeitura();
            imagem1->armazenarPixel();
            imagem1->pegarNumeroMagico();
            imagem1->testeNumeroMagico();
            imagem1->pegarPosicaoInicial();
            imagem1->pegarAltura();
            imagem1->pegarLargura();
            imagem1->calculaTamanhoTotal();
            imagem1->extrairMensagem();
            imagem1->pegarValorMaximo();
            imagem1->fecharImagem();
            delete (imagem1);
        break;
        case 2:
            cout<<"=== Ppm ==="<<endl;
            Ppm *imagem2;
            imagem2=new Ppm();
            imagem2->digiteEndereco();
            imagem2->abrirImagemLeitura();
            imagem2->armazenarPixel();
            imagem2->pegarNumeroMagico();
            imagem2->testeNumeroMagico();
            imagem2->pegarAltura();
            imagem2->pegarLargura();
            imagem2->calculaTamanhoTotal();
            imagem2->pegarValorMaximo();
            imagem2->escolhaFiltro();
            imagem2->fecharImagem();
            delete (imagem2);
        break;
        default:
            cout<<"=== Opção inválida ==="<<endl;
        break;
      }
      cout<<endl;
  }while(menu!=0);
};
