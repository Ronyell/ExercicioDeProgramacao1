#include "ppm.hpp"
using namespace std;
Ppm::Ppm(){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco("  ");
  setNumeroMagico("  ");
}
Ppm::Ppm(string endereco){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco(endereco);
  setNumeroMagico("  ");
}
Ppm::~Ppm(){

}
int Ppm::calculaPosicaoInicial(){
  int contador1=0,auxiliar;
  for(int contador=0;getPixel()->at(contador)&&contador1<3;contador++){
       if(getPixel()->at(contador)=='\n'){
         contador1++;
       }
       if(getPixel()->at(contador)=='#'){
         contador1--;
       }
       if (contador1==3){
         auxiliar=contador;
       }
     }
  return auxiliar+1;
}
void Ppm::transformaRed(){
  fstream red;
  string endereco;
  for(int contador=0;contador<(int)strlen(getEndereco().c_str())-3;contador++){
    if(contador<(int)strlen(getEndereco().c_str())-4){
      endereco+=getEndereco()[contador];
    }
    else{
      endereco+="R.ppm";
    }
  }
  red.open(endereco.c_str(),ios::binary|ios::out);
  if (!red.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
      int contador1=1;
      red<<getNumeroMagico()<<'\n'<<getLargura()<<" "<<getAltura()<<'\n'<<getValorMaximo()<<endl;
      for(int contador=calculaPosicaoInicial();contador<(int)getPixel()->size();contador++){
          if (contador1!=1){
            red<<(char)0;
            contador1++;
            if (contador1==4){
              contador1=1;
            }
          }
          else{
            red<<(char)getPixel()->at(contador);
            contador1++;
          }
      }
        cout<<"A aplicação do filtro R foi salva em um arquivo no diretório: "<<endereco<<endl;
      red.close();
  }
}
void Ppm::transformaGreen(){
  fstream green;
  string endereco;
  for(int contador=0;contador<(int)strlen(getEndereco().c_str())-3;contador++){
    if(contador<(int)strlen(getEndereco().c_str())-4){
      endereco+=getEndereco()[contador];
    }
    else{
      endereco+="G.ppm";
    }
  }
  green.open(endereco.c_str(),ios::binary|ios::out);
  if (!green.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
  int contador1=1;
      green<<getNumeroMagico()<<'\n'<<getLargura()<<" "<<getAltura()<<'\n'<<getValorMaximo()<<endl;
      for(int contador=calculaPosicaoInicial();contador<(int)getPixel()->size();contador++){
          if (contador1!=2){
            green<<(char)0;
            contador1++;
            if (contador1==4){
              contador1=1;
            }
          }
          else{
            green<<(char)getPixel()->at(contador);
            contador1++;
          }
      }
        cout<<"A aplicação do filtro G foi salva em um arquivo no diretório: "<<endereco<<endl;
      green.close();
  }
}

void Ppm::transformaBlue(){
  fstream blue;
  string endereco;
  for(int contador=0;contador<(int)strlen(getEndereco().c_str())-3;contador++){
    if(contador<(int)strlen(getEndereco().c_str())-4){
      endereco+=getEndereco()[contador];
    }
    else{
      endereco+="B.ppm";
    }
  }
  blue.open(endereco.c_str(),ios::binary|ios::out);
  if (!blue.is_open()) {
    cout<<"Erro, não foi possível abrir arquivo.";
  }
  else{
      int contador1=1;
      blue<<getNumeroMagico()<<'\n'<<getLargura()<<" "<<getAltura()<<'\n'<<getValorMaximo()<<endl;
      for(int contador=calculaPosicaoInicial();contador<(int)getPixel()->size();contador++){
          if (contador1!=3){
            blue<<(char)0;
            contador1++;
          }
          else{
            blue<<(char)getPixel()->at(contador);
            contador1=1;
          }
      }
      cout<<"A aplicação do filtro B foi salva em um arquivo no diretório: "<<endereco<<endl;
      blue.close();
    }
}
void Ppm::testeNumeroMagico(){
  if (strcmp(getNumeroMagico().c_str(),"P6")){
    cout<<"Erro, arquivo não é Ppm!"<<endl;
    exit(0);
  }
}
void Ppm::escolhaFiltro(){
  int menu,opcao;
  do{
  cout<<"============================================================================"<<endl;
  cout<<"Escolha a cor do filtro:\n"<<"1 - Red\n"<<"2 - Green\n"<<"3 - Blue"<<endl;
  cin>>menu;
  switch(menu){
    case 1:
      transformaRed();
    break;
    case 2:
      transformaGreen();
    break;
    case 3:
      transformaBlue();
    break;
    default:
      cout<<"=== Opção inválida ==="<<endl;
    break;
  }
  cout<<"Deseja aplicar outro filtro?"<<"\n1 - Sim"<<"\n0 - Não"<<endl;
  cin>>opcao;
}while(opcao!=0);
}
