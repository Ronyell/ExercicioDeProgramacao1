#include "imagem.hpp"
using namespace std;

 Imagem::Imagem(){
   setAltura(0);
   setLargura(0);
   setTamanhoTotal(0);
   setEndereco(" ");
   setNumeroMagico("  ");
 }
Imagem::Imagem(string endereco){
  setAltura(0);
  setLargura(0);
  setTamanhoTotal(0);
  setEndereco(endereco);
  setNumeroMagico("  ");
}
Imagem::~Imagem(){
}

int Imagem::getAltura(){
  return this->altura;
}
int Imagem::getLargura(){
  return this->largura ;
}
int Imagem::getTamanhoTotal(){
  return this->tamanhoTotal;
}
string Imagem::getEndereco(){
  return this->endereco;
}
fstream *Imagem::getArquivo(){
  return &this->arquivo;
}
string *Imagem::getPixel(){
  return &this->pixel;
}
void Imagem::setAltura(int altura){
  this->altura=altura;
}
void Imagem::setLargura(int largura){
  this->largura=largura;
}
void Imagem::setTamanhoTotal(int tamanhoTotal){
  this->tamanhoTotal=tamanhoTotal;
}
void Imagem::setEndereco(string endereco){
  this->endereco=endereco;
}
void Imagem::abrirImagemLeitura(){
  this->arquivo.open(this->endereco.c_str(),ios::binary|ios::in);
  validarEndereco();
}
void Imagem::fecharImagem(){
  this->arquivo.close();
}
void Imagem::validarEndereco(){
  if(!this->arquivo){
    cout<<"Erro,arquivo não pode ser aberto!"<<endl;
    exit(0);
  }
}
void Imagem::armazenarPixel(){
  char pixel;
  while(this->arquivo.get(pixel)){
    this->pixel.push_back(pixel);
  }
}
string Imagem::getNumeroMagico(){
  return this->numeroMagico;
}
void Imagem::setNumeroMagico(string numeroMagico){
  this->numeroMagico=numeroMagico;
}
void Imagem::pegarNumeroMagico(){
  for(int contador=0;getPixel()->at(contador)!='\n';contador++){
  numeroMagico[contador]=getPixel()->at(contador);
}
setNumeroMagico(numeroMagico);
}
void Imagem::pegarLargura(){
  int contador=0;
  string largura;
  for(int contador1=0;contador1<(int)getPixel()->size() && contador<2;contador1++){
    if (getPixel()->at(contador1)=='\n'){
      contador++;
    }
    if (getPixel()->at(contador1+1)=='#'){
      contador--;
    }
    if(contador==1){
      for(int contador2=contador1+1;getPixel()->at(contador2)!=' ';contador2++){
        largura+=getPixel()->at(contador2);
      }
      this->largura=stoi(largura);
      contador++;
    }
  }
}
void Imagem::pegarAltura(){
  int contador=0;
  string altura;
  for(int contador1=0;contador1<(int)getPixel()->size() && contador<2;contador1++){
    if (getPixel()->at(contador1)=='\n'){
      contador++;
    }
    if (getPixel()->at(contador1+1)=='#'){
      contador--;
    }
    if(contador==1){
      for(int contador2=contador1+1;getPixel()->at(contador2)!=' ';contador2++){
          contador1++;
      }
      for(int contador2=contador1+1;getPixel()->at(contador2)!='\n';contador2++){
        altura+=getPixel()->at(contador2);
      }
      this->altura=stoi(altura);
      contador++;
    }
  }
}
void Imagem::calculaTamanhoTotal(){
  this->tamanhoTotal=this->largura*this->altura;
}
void Imagem::setValorMaximo(int valorMaximo){
  this->valorMaximo=valorMaximo;
}
int Imagem::getValorMaximo(){
  return this->valorMaximo;
}
void Imagem::pegarValorMaximo(){
  int contador=0;
  string valorMaximo;
  for(int contador1=0;contador1<(int)getPixel()->size() && contador<3;contador1++){
    if (getPixel()->at(contador1)=='\n'){
      contador++;
    }
    if (getPixel()->at(contador1+1)=='#'){
      contador--;
    }
    if(contador==2){
      for(int contador2=contador1+1;getPixel()->at(contador2)!=' ';contador2++){
        valorMaximo+=getPixel()->at(contador2);
      }
      this->valorMaximo=stoi(valorMaximo);
      contador++;
    }
  }
}
void Imagem::digiteEndereco(){
  string endereco;
  cout<<"Digite o diretório da imagem: "<<endl;
  cin>>endereco;
  this->endereco=endereco;
}
