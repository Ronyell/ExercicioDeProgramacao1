=====================================================README=========================================================
bin: Pasta que contém os arquivos binários.
inc: Pasta que contém os arquivos headers.
src: Pasta que contém as implementações dos heades.
obj: Pasta que receberá os arquivos obj.
doc : Pasta que contém qualquer informação extra, como as imagens.

make: compila
make run: executa

O programa contém instruções, apenas siga-o.

Observações:
Digite "doc/imagem1.pgm" para usar a imagem pgm (após escolha do usuário). A mensagem será salva em "doc/imagem1.Mensagem.pgm".
Digite "doc/segredo.ppm" para usar a imagem ppm (após escolha do usuário). As imagens com aplicação dos filtros RGB serão salvas em (de acordo com escolha do usuário) ou "doc/segredoR.ppm" ou "doc/segredoG.ppm" ou "doc/segredoB.ppm"
Caso digite um diretório inválido ou um arquivo incompatível com o selecionado o programa irá encerrar.

make clean: limpa a pasta bin e obj
