#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

class Imagem{
private:
  int altura;
  int largura;
  int tamanhoTotal;
  string endereco;
  fstream arquivo;
  string pixel;
  string numeroMagico;
  int valorMaximo;
public:
  //Construtor/Destrutor
  Imagem();
  Imagem(string endereco);
  virtual ~Imagem();
  //Métodos de acesso get
  int getAltura();
  int getLargura();
  int getTamanhoTotal();
  string getEndereco();
  fstream *getArquivo();
  string *getPixel();
  string getNumeroMagico();
  int getValorMaximo();
  //Métodos
  void abrirImagemLeitura();//Método de abrir arquivo imagem
  void fecharImagem();//Método de fechar arquivo imagem
  void validarEndereco();//Método para testar de o arquivo foi aberto
  void armazenarPixel();//Método para pegar pixels do arquivo
  void pegarNumeroMagico();//Método para pegar numero mágico do arquivo
  void pegarAltura();//Método para pegar altura do arquivo
  void pegarLargura();//Método para pegar largura do arquivo
  void pegarValorMaximo();//Método para pegar altura do arquivo
  void calculaTamanhoTotal();//Método para calcular o tamanho total do arquivo
  void digiteEndereco();//Método para pegar o endereco do arquivo
  virtual void testeNumeroMagico()=0;//Método virtual puro que deixa a classe abstrata
  virtual int calculaPosicaoInicial()=0;//Método virtual puro que deixa a classe abstrata
protected:
  //Métodos de acesso set
  void setAltura(int altura);
  void setLargura(int largura);
  void setTamanhoTotal(int tamanhoTotal);
  void setEndereco(string endereco);
  void setNumeroMagico(string numeroMagico);
  void setValorMaximo(int valorMaximo);
};
#endif
