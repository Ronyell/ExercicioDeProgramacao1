#ifndef PPM_HPP
#define PPM_HPP
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include "imagem.hpp"

using namespace std;
class Ppm : public Imagem{
public:
  //Construtor/Destrutor
  Ppm();
  Ppm(string endereco);
  ~Ppm();
  //Métodos
  int calculaPosicaoInicial();//Método que calcula a posição inicial da imagem
  void transformaRed();//Método que aplica o filtro Red na imagem
  void transformaGreen();//Método que aplica o filtro Green na imagem
  void transformaBlue();//Método que aplica o filtro Blue na imagem
  void testeNumeroMagico();//Método verifica se o número mágico é P6
  void escolhaFiltro();//Método que solicita o usuário a escolha do filtro aplicado
};
#endif
